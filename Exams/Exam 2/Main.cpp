
// Exam 3
// Vincent Zaragoza

// Extra credit attempted

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

double FindAverage(double numbers[])
{
	double average=0;

	for (int j = 0; j < 5; j++)
	{
		average = average + numbers[j];
	}
	average = average / 5;
	return average;
}

double FindHighest(double numbers[])
{
	double highest=0;
	highest = numbers[0];

	for (int i = 0; i < 5; i++)
	{
		if (highest < numbers[i])
		{
			highest = numbers[i];
		}
	}
	return highest;
}

double FindLowest(double numbers[])
{
	double lowest = 0;
	lowest = numbers[0];

	for (int i = 0; i < 5; i++)
	{
		if (lowest > numbers[i])
		{
			lowest = numbers[i];
		}
	}

	return lowest;
}

void PrintPerson(double numbers[], ostream &os = cout)
{
	os << "Numbers: \n";
	for (int i = 4; i >= 0; i--)
	{
		os << numbers[i] << "\n";//Prints array backwords
	}
	os << "Average: " << FindAverage(numbers) << "\n";
	os << "Highest: " << FindHighest(numbers) << "\n";
	os << "Lowest: " << FindLowest(numbers) << "\n";
}


int main()
{
	double input[5];
	char redo = 'n';
	

	cout << "Enter 5 numbers:\n";
	for (int i = 0; i < 5; i++)
	{
		cin >> input[i];
	}
	
	PrintPerson(input, cout);

	cout << "Would you like to save data(y/n): ";
	cin >> redo;

	while (redo != 'n')
	{
		if (redo == 'y')
		{

			string path = "C:\\Users\\Public\\Numbers.txt";
			
			ofstream ofs(path);
			PrintPerson(input, ofs);
			ofs.close();

			cout << "Saved to " << path;

			break;
		}
		else
		{
			cout << "Invalid input!\n";
		}
	}
	

	_getch();
	return 0;
}
