//Playing Cards
//Vincent Zaragoza

#include <conio.h>
#include <iostream>
#include <string>

using namespace std;



enum Rank 
{ 
	TWO = 2,THREE,FOUR,FIVE,SIX,SEVEN,EIGHT,NINE,TEN,JACK,QUEEN,KING,ACE
};

enum Suit
{
	HEART, DIAMOND, SPADE, CLUB
};

struct Card
{		
	Rank rank;
	Suit suit;
};

void PrintCard(Card card) 
{
	if (card.suit == SPADE) cout << card.rank << " of "<< "Spades";
	if (card.suit == DIAMOND) cout << card.rank << " of " << "Diamonds";
	if (card.suit == HEART) cout << card.rank << " of " << "Hearts";
	if (card.suit == CLUB) cout << card.rank << " of " << "Clubs";
}

Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank) {
		PrintCard(card1);
		return card1;
	}
	else {
		PrintCard(card2);
		return card2;
	}
}

int main()
{
	Card card1;
	card1.suit = HEART;
	card1.rank = TWO;


	Card card2;
	card2.rank = FOUR;
	card2.suit = SPADE;

	HighCard(card1, card2);


	_getch();
	return 0;
}