#pragma once

#include "Animal.h"
#include <iostream>


class Dog : public Animal
{
public:
	Dog()
	{
		SetAgeScale(7);
	}


	virtual void Speak()
	{
		std::cout << GetName() << " says woof!\n";
	}

	virtual void Move()
	{
		//Animal::Move();
		std::cout << GetName() << " is running!\n";
	}
};