#pragma once

#include <iostream>
#include <string>
#include "Animal.h"

using namespace std;

class Person
{
private:
	string m_name;
	Animal *m_pPet = nullptr;

public:
	//Accessor methods
	string GetName() { return m_name; }
	Animal *GetPet() { return m_pPet; }

	//Mutator methods
	void SetName(string name) { m_name = name; }
	void SetPet(Animal *pPet) { m_pPet = pPet; }

	//Other methods
	void Display()
	{
		cout << GetName();
		if (GetPet())
		{
			cout << " owns: ";
			GetPet()->Display();
		}
		else
		{
			cout << " doesn't own any pets.\n";
		}
	}

	void SummonPet()
	{
		//if (m_pPet) delete m_pPet;
		//m_pPet = new Animal;
		//m_pPet->SetName("Devil Spawn");
		//m_pPet->SetAge(0);
	}
};