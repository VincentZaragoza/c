#include "Animal.h"

int Animal::s_count = 0;
//Constructors
Animal::Animal()//Automaticaly Sets name and age when an animal is created with no variables passed
{
	SetName("Frank");
	SetAge(0);
	s_count++;
}
Animal::Animal(string name, int age)
{
	SetName(name);
	SetAge(age);
	s_count++;
}

void  Animal::Display()
{
	std::cout << GetName() << " is " << GetAge();
}