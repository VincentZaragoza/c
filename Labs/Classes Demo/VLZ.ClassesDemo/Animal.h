#pragma once

#include <iostream>
#include <string>

using namespace std;


class Animal
{

private:
	//Fields (class variables)
	int m_age;
	string m_name;
	int m_ageScale = 1;
	static int s_count;

protected:
	virtual void SetAgeScale(int scale) { m_ageScale = 1; }

public:	

	//Constructors
	Animal();//Automaticaly Sets name and age when an animal is created with no variables passed

	Animal(string name, int age);
	
	//Deconstructor
	~Animal()
	{
		//used to delete an object created dynamically in this class
		//EX.
		//delete m_pMouse; //if we had ====>  m_pMouse = new Mouse;
	}

	//Accessor methods
	virtual int GetAge() { return m_age * m_ageScale; }
	virtual string GetName() { return m_name; }
	static int GetCount() { return s_count; }

	//Mutator methods
	virtual void SetAge(int age) { m_age = age; }
	virtual void SetName(string name) { m_name = name; }

	//Other methods
	virtual void Display();
	
	virtual void Move() = 0;
};