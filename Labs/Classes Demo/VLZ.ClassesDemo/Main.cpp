// Classes Demo
// Vincent Zaragoza

#include <conio.h>
#include "Person.h"
#include "Dog.h"


using namespace std;

int main()
{
	//Animal a("Crusty",5);//Setting values using overloaded constructor
	//Animal *pB = new Animal;

	//a.SetName("Crusty");
	//a.SetAge(4);
	//a.Display();
	//a.Move();

	//pB->SetName("Charles");
	//pB->SetAge(2);
	//pB->Display();

	Person t;
	t.SetName("Tristen");
	//t.SetPet(pB);
	t.Display();

	t.SummonPet();
	t.Display();

	cout << "Count: " << Animal::GetCount() << "\n";


	Dog d;
	d.SetName("Elmer");
	d.Move();

	cout << "Count: " << Animal::GetCount() << "\n";

	Animal*pDog = new Dog;
	pDog->Move();

	cout << "Count: " << Animal::GetCount() << "\n";

	_getch();
	return 0;
}
