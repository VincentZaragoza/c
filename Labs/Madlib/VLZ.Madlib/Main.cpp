#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>
using namespace std;

const string path = "C:\\temp\\test.txt";

void PrintOutMadlib(string *pmML, ostream &os)
{
	os << "\nMy name is " << pmML[0] << " I am a " << pmML[1] << " that has " << pmML[2] << " fur with " << pmML[3] << " spots on his " << pmML[4] <<
		" I have a " << pmML[5] << " shaped like " << pmML[6] << " which i use for " << pmML[7] << " I weigh more than " << pmML[8] << " pounds and I am " << pmML[9] << " feet tall.\n";
}
int main()
{
	string words[10] = { " Name", " Animal", " Color", " Another Color", " Body Part", " Another Body Part", " Noun", " Verb", " Number", " Another Number" };
	string input[10];

	for (int i =0; i < 10; i++)
	{
		cout << "Enter"<< words[i] << ":";
		cin >> input[i];
	}

	PrintOutMadlib(input,cout);

	char redo = 'n';
	cout << "Do you want to save this file?:";
	cin >> redo;
	
	if (redo == 'y')
	{
		//ofstream ofs;
		//ofs.open(path);//Creates and opens file stream in two lines 
		ofstream ofs(path);//Creates and opens file stream in one line

		PrintOutMadlib(input, ofs);
		ofs.close();
	}

	//Read in file
	//ifstream ifs(path);
	//string line;
	//while (getline(ifs, line))
	//{			
	//	cout << line << "\n";
	//}
	//ifs.close();
	   
	_getch();
	return 0;
}