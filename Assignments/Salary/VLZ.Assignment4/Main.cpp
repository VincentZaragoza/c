	// Assignment 4 - Salary
// Vincent Zaragoza

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

struct employee 
{
	int id;
	string fName;
	string lName;
	float payRate;
	float hours;
};

int main()
{
	int numPeople = 0;
	float *grossPay = new float[numPeople];
	float totGrossPay = 0;

	cout << "How many workers do you have?: ";
	cin >> numPeople;

	employee *workers= new employee[numPeople];

	for (int i = 0; i < numPeople; i++)
	{
		cout << "Enter information of employee " << i+1 <<"\n";
		cout << "ID: ";
		cin >> workers[i].id;		
		cout << "Pay Rate: ";
		cin >> workers[i].payRate;
		cout << "hours: ";
		cin >> workers[i].hours;
		cout << "first name: ";
		cin >> workers[i].fName;
		cout << "last name: ";
		cin >> workers[i].lName;
	}


	for (int i = 0;  i < numPeople; i++)
	{
		grossPay[i] = workers[i].payRate * workers[i].hours;
		totGrossPay = grossPay[i] + totGrossPay;
	}

	cout << "\nEmployees\n";
	for (int i = 0; i < numPeople; i++)
	{				
		cout << "ID:" << workers[i].id<<"\n";
		cout << "Pay Rate:" << workers[i].payRate<< "\n";
		cout << "Hours:" << workers[i].hours << "\n";
		cout << "Gross Pay:" << grossPay[i] << "\n";
		cout << "First Name:" << workers[i].fName << "\n";
		cout << "Last Name:" << workers[i].lName << "\n\n";
	}
	cout << "Total Gross pay:" << totGrossPay;

	_getch();
	return 0;
}