//Vincent Zaragoza
//Assignment 2
//2/2/2019

#include <iostream>
#include <conio.h>
#include <exception>
#include <string>

float add(float x, float y) 
{
	float result = 0;

	result = x + y;
	std::cout << "The sum of " << x << " + " << y << " is: " << result ;

	return result;
}

float Subtract(float x, float y)
{
	float result = 0;

	result = x - y;
	std::cout << "The difference of " << x << " - " << y << " is: " << result;

	return 0;
}

float Multiply(float x, float y)
{
	float result = 0;

	result = x * y;
	std::cout << "The product of " << x << " * " << y << " is: " << result;

	return 0;
}

bool Divide(float x, float y, float &result)
{
	if (y != 0) {
		result = x / y;
		std::cout << "The difference between " << x << " and " << y << " is:";
		return true;
	}
	else {
		return false;
	}	
}

float Pow(float base, int exponent)
{
	float result = 1;	

	std::cout << base << "^" << exponent << " = ";

	while (exponent != 0) {
		result *= base;
		--exponent;
	}
	std::cout << result;
	return 0;
}

bool Confirm(std::string confirm) {
	std::string reconfirm;
	if(confirm == "y"||confirm == "Y") {		
		return true;
	}
	else if(confirm == "n"||confirm == "N") {
		return false;
	}
	else
	{
		std::cout << "Invalid input"; 
		std::cout << "\nPress \"y\" to continue \"n\" to quit\n";
		std::cin >> reconfirm;
		Confirm(reconfirm);
	}
}


int main()
{
	float x, y;
	char operation;
	std::string confirm;
	bool redo;
	do {
		
		std::cout << "Please enter an operation which you would like to calculate(+,-,*,/,^)\n";
		std::cin >> operation;

		switch(operation){		
		case'+':
			std::cout << "Please enter two numbers to apply your requested operation(" << operation << "):\n1st num:";
			std::cin >> x; 
			std::cout << "2nd num:";
			std::cin >> y;
			
			add(x, y);
			
			std::cout << "\nPress \"y\" to continue \"n\" to quit\n";
			std::cin >> confirm;
			redo = Confirm(confirm);
			break;

		case'-':
			std::cout << "Please enter two numbers to apply your requested operation(" << operation << "):\n1st num:";
			std::cin >> x;
			std::cout << "2nd num:";
			std::cin >> y;

			Subtract(x, y);

			std::cout << "\nPress \"y\" to continue \"n\" to quit\n";
			std::cin >> confirm;
			redo = Confirm(confirm);
			break;
		case'*':
			std::cout << "Please enter two numbers to apply your requested operation(" << operation << "):\n1st num:";
			std::cin >> x;
			std::cout << "2nd num:";
			std::cin >> y;
			Multiply(x, y);

			std::cout << "\nPress \"y\" to continue \"n\" to quit\n";
			std::cin >> confirm;
			redo = Confirm(confirm);
			break;
		case'/':
			float result;

			std::cout << "Please enter two numbers to apply your requested operation(" << operation << "):\n1st num:";
			std::cin >> x;
			std::cout << "2nd num:";
			std::cin >> y;

			if (Divide(x, y,result)) {
				std::cout << result;
				std::cout << "\nPress \"y\" to continue \"n\" to quit\n";
				std::cin >> confirm;
				redo = Confirm(confirm);
			}
			else
			{
				std::cout << "You cannot divide a number by Zero";
				std::cout << "\nPress \"y\" to continue \"n\" to quit\n";
				std::cin >> confirm;
				redo = Confirm(confirm);
			}
			break;
		case'^':
			std::cout << "Please enter two numbers to apply your requested operation(" << operation << "):\n1st num:";
			std::cin >> x;
			std::cout << "2nd num:";
			int exponent;
			std::cin >> exponent ;
			

			Pow(x,exponent);

			std::cout << "\nPress \"y\" to continue \"n\" to quit\n";
			std::cin >> confirm;
			redo = Confirm(confirm);
			break;
		default:
			std::cout << "Invalid input\n";
			std::cout << "Press \"y\" to continue \"n\" to quit\n";
			std::cin >> confirm;
			redo = Confirm(confirm);
			break;
		}
	}while (redo);

}